package de.ur.iw.lsl_processor;

import com.google.auto.value.AutoValue;

@AutoValue
abstract class DataPoint {
	public static DataPoint create(double timestamp, float value) {
		return new AutoValue_DataPoint(timestamp, value);
	}

	public abstract double timestamp();

	public abstract float value();
}