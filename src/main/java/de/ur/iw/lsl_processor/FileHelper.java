package de.ur.iw.lsl_processor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.common.io.Resources;


public class FileHelper {

	public static Path getCurrentWorkingDirectory() {
		return Paths.get("").toAbsolutePath();
	}

	public static InputStream getResourceAsStream(String name) throws IOException {
		return Resources.asByteSource(Resources.getResource(name)).openStream();
	}
}
