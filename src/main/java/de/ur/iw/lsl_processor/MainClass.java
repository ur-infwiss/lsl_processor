package de.ur.iw.lsl_processor;

import java.util.prefs.Preferences;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Hello world!
 *
 */
public class MainClass {
	/**
	 * @return the <code>Preferences</code> instance representing this program.
	 */
	public static Preferences preferences() {
		return Preferences.userNodeForPackage(MainClass.class);
	}

	public static void main(String[] args) {
		Options options = new Options();
		{
			Option predicate = new Option("p", "predicate", true,
					"XPath 1.0 Predicate for the <info> node of resolved streams.");
			predicate.setRequired(true);
			options.addOption(predicate);

			Option channel = new Option("c", "channel", true, "Channel index within stream, begins with 0.");
			channel.setRequired(false);
			options.addOption(channel);

			Option timeWindowSize = new Option("w", "timewindow", true,
					"Size of the displayed time window in seconds.");
			timeWindowSize.setRequired(false);
			options.addOption(timeWindowSize);

			Option ignoreTimeCorrection = new Option("t", "ignoretimecorrection", true,
					"Can be used to ignore LSL's time_correction mechanism and assume that data is from now.");
			ignoreTimeCorrection.setRequired(false);
			options.addOption(ignoreTimeCorrection);
		}

		try {

			CommandLine cmd = new DefaultParser().parse(options, args);
			String predicate = cmd.getOptionValue("p");
			int channel = Integer.parseInt(cmd.getOptionValue("c", "0"));
			double timeWindowSize = Double.parseDouble(cmd.getOptionValue("w", "30"));
			boolean ignoreTimeCorrection = Boolean.parseBoolean(cmd.getOptionValue("t", "False"));

			StreamDataReader reader = new StreamDataReader(predicate, channel, ignoreTimeCorrection);
			RMSSD rmssd = new RMSSD(reader, timeWindowSize);

			Thread readerThread = new Thread(reader);
			readerThread.start();

			Thread processorThread = new Thread(rmssd);
			processorThread.start();
			
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("<program-name>", options);
			System.exit(1);
		}

	}

	public static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}
}
