package de.ur.iw.lsl_processor;

import static de.ur.iw.lsl_processor.MainClass.sleep;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

import edu.ucsd.sccn.LSL;
import edu.ucsd.sccn.LSL.StreamInfo;
import edu.ucsd.sccn.LSL.StreamOutlet;

public class RMSSD implements Runnable {
	private final StreamDataReader reader;
	private final Queue<DataPoint> slidingWindow = new ArrayDeque<>();
	private final double windowSize;

	public RMSSD(StreamDataReader reader, double windowSize) {
		this.reader = reader;
		this.windowSize = windowSize;
	}

	@Override
	public void run() {

		// wait until first stream is found to get metadata from
		StreamInfo inletInfo = null;
		do {
			inletInfo = reader.getStreamInfo();
			sleep(1);
		} while (inletInfo == null);

		StreamInfo outletInfo = new StreamInfo("RMSSD of " + inletInfo.name(), "RMSSD", 1, inletInfo.nominal_srate(),
				LSL.ChannelFormat.float32);
		try {
			StreamOutlet outlet = new StreamOutlet(outletInfo);

			float[] rmssd = new float[1];
			do {
				BlockingQueue<DataPoint> queue = reader.getDataQueue();
				DataPoint data = queue.poll(2000, TimeUnit.MILLISECONDS);
				if (data == null || data.value() == 0) {
					sleep(1);
					continue;
				}

				System.out.println("received: " + data.value());

				slidingWindow.add(data);
				while (slidingWindow.size() >= 2
						&& (slidingWindow.element().timestamp() + windowSize < data.timestamp())) {
					slidingWindow.poll();
				}

				if (slidingWindow.size() >= 2) {
					double sum = 0;
					PeekingIterator<DataPoint> iter = Iterators.peekingIterator(slidingWindow.iterator());
					do {
						DataPoint current = iter.next();
						if (!iter.hasNext()) {
							break;
						}
						DataPoint next = iter.peek();

						double diff = current.value() - next.value();
						sum += diff * diff;
					} while (true);

					rmssd[0] = (float) Math.sqrt(sum / (slidingWindow.size() - 1));
					System.out.println("rmssd: " + rmssd[0]);

					outlet.push_sample(rmssd);
				}
			} while (true);

		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}
}
