package de.ur.iw.lsl_processor;

import static de.ur.iw.lsl_processor.MainClass.sleep;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import edu.ucsd.sccn.LSL;
import edu.ucsd.sccn.LSL.LostException;
import edu.ucsd.sccn.LSL.StreamInfo;
import edu.ucsd.sccn.LSL.TimeoutException;

public class StreamDataReader implements Runnable {

	public StreamDataReader(String predicate, int channel, boolean ignoreTimeCorrection) {
		this.predicate = predicate;
		this.channel = channel;
		this.ignoreTimeCorrection = ignoreTimeCorrection;
	}

	private static final int MIN_QUEUE_SIZE = 100;

	private final String predicate;
	private final int channel;
	private final boolean ignoreTimeCorrection;
	private volatile BlockingQueue<DataPoint> dataQueue = new ArrayBlockingQueue<>(MIN_QUEUE_SIZE);
	private volatile StreamInfo streamInfo = null;

	@Override
	public void run() {
		Thread.currentThread().setName("LSL stream reader");
		do {
			double timeCorrection = 0.0;
			long lastTimeCorrectionUpdate = 0;
			LSL.StreamInlet inlet = null;
			try {
				System.out.println("resolve_stream(" + predicate + ")");
				LSL.StreamInfo[] results = LSL.resolve_stream(predicate);
				if (results.length > 0) {
					inlet = new LSL.StreamInlet(results[0]);
					streamInfo = inlet.info();
					Thread.currentThread()
							.setName("LSL stream reader - " + streamInfo.name() + " - channel " + channel);
					// System.out.println(info.as_xml());
					try {
						try {
							inlet.time_correction(3.0);
						} catch (TimeoutException e) {
							System.err.println("time_correction (initial): " + e.getMessage());
							// TODO: this starts to happen when a stream outlet is killed and restarted - i
							// don't know why
						}
						final int channelCount = streamInfo.channel_count();
						// queue size: 1 second or at least 100 samples
						final int sampleCount = Math.max((int) Math.ceil(streamInfo.nominal_srate()), MIN_QUEUE_SIZE);
						BlockingQueue<DataPoint> newQueue = new ArrayBlockingQueue<>(sampleCount);
						dataQueue.drainTo(newQueue);
						dataQueue = newQueue;
						final double timeout = 0.01; // we try to poll data around 100 times per second
						float[] data_buffer = new float[channelCount * sampleCount];
						double[] timestamp_buffer = new double[sampleCount];

						do {
							int valuesWritten = inlet.pull_chunk(data_buffer, timestamp_buffer, timeout);
							int samplesWritten = valuesWritten / channelCount;
							if (samplesWritten > 0) {
								if (ignoreTimeCorrection) {
									double youngestTimestamp = timestamp_buffer[samplesWritten - 1];
									timeCorrection = LSL.local_clock() - youngestTimestamp;
								} else if (lastTimeCorrectionUpdate + 1000000000l < System.nanoTime()) {
									try {
										lastTimeCorrectionUpdate = System.nanoTime();
										timeCorrection = inlet.time_correction(timeout);
									} catch (TimeoutException e) {
										System.err.println("time_correction: " + e.getMessage());
										// TODO: this starts to happen when a stream outlet is killed and restarted - i
										// don't know why
									}
								}
								for (int i = 0; i < samplesWritten; i++) {
									double x = timestamp_buffer[i] + timeCorrection;
									float y = data_buffer[channelCount * i + channel];
									while (!dataQueue.offer(DataPoint.create(x, y))) {
										// full, try again
										sleep(10);
									}
									;
								}
							}
							sleep((long) (timeout * 1000));
						} while (true);
					} finally {
						System.out.println("Closing inlet " + streamInfo.name());
						streamInfo = null;
						inlet.close_stream();
						inlet.close();
						inlet = null;
					}

				} else {
					System.err.println("No stream found matching '" + predicate + "' ...");
				}
			} catch (LostException e) { // stream source was lost
				System.err.println("LostException: " + e.getMessage());
			} catch (Exception e) { // unexpected
				System.err.println("unexpected: " + e.getMessage());
				e.printStackTrace();
			}

			// wait a bit, then search again
			sleep(1000);
			// now try to re-discover a suitable stream

		} while (true);
	}

	public BlockingQueue<DataPoint> getDataQueue() {
		return dataQueue;
	}

	public String getPredicate() {
		return predicate;
	}

	public int getChannel() {
		return channel;
	}

	public StreamInfo getStreamInfo() {
		return streamInfo;
	}

}
